# Coding Challenge Submission
This is the submission of the coding challenge.

This project uses:

* Ruby version - 2.7.0

* Rails version - 6.0.3.1

* External Gems integrated
    * rspec for testing
    * factorybot for test data stubbing
    * faker to generate random user data and support requests

* Packages included with webpack
    * bootstrap
    * bootswatch
    * styled-components
    * axios
    * font-awesome

### Installation procedure
1. clone the project from the Gitlab repo

2. cd into the folder

3. run `bundle install` to install all dependencies

4. run `rails doublegdp:setup_database` to create, migrate and seed the database, 

5. run `bin/rails spec` to verify all tests pass

6. run `rails s` and take it for a spin.



### How did you decide on the technical and architectural choices used as part ofyour solution?

The technical and architectural choices made were in accordance to specifications given for the project and the allowable  constraints by the framework being used for development purposes. Other decisions made were of gems included such as
    * rspec - easy of use and easily readable syntax.
    * factorybot - prevents duplicity in setting up test data
    * faker - simplifies the process of coming up with test data

### Are there any improvements you could make to your submission?
 * client side validation
 * better front end organisation and design
 * better test coverage
 
 
### What would you do differently if you were allocated more time?
 I will add all the improvements as stated above and implement a full CRUD.





