import React, { Component } from 'react'
import axios from 'axios'

class AddTask extends Component {
    constructor(props){
        super(props)
        console.log(props)
    }

    state = {
        description: '',
        avatar_url: '',
        errorMessage: ''
    }

    change = e => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    onSubmit = e => {
        e.preventDefault();
        // console.log(this.state)
        // this.props.onSubmit(this.state)
        axios.post('/tasks.json', {task: {avatar_url: this.state.avatar_url, description: this.state.description, username: "Jaja of Opobo"}})
        .then(data => {
            alert("Task has been saved successfully")
            this.props.triggerBackHomeState(this.state)
        })
        .catch(error => {
            console.log(error)
            this.setState({errorMessage: error.message});
        })
    }

    render(){
        return(
            <div className="jumbotron">
                { this.state.errorMessage &&
                <p className="error"> { this.state.errorMessage } </p> }
            <div className="col-xs-6">
            <label>Description: </label>
            <br/>
            <input
                className="form-control"
                type='text'
                name='description'
                value={this.state.description}
                onChange={e => this.change(e)}
                />
            </div>
        <br/>
            <div className="col-xs-6">
            <label>Avatar Url: </label>
            <br/>
            <input
                className="form-control"
                type='text'
                name='avatar_url'
                value={this.state.avatar_url}
                onChange={e => this.change(e)}
                />
            </div>
        <button className="btn btn-primary mt-4" onClick={(e)=> this.onSubmit(e)}>Add</button>
      </div>
        )
    }
}

export default AddTask