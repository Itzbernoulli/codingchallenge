import React, { Component } from 'react'
import AddTask from './AddTask'
import Header from './Header'

class Page extends Component {
    constructor(props){
        super(props)
        console.log(props)
    }
    // onSubmit = (fields) => {
    //     console.log("App got the fields", fields)
    // }

    render(){
        return(
            <div>
                <div><Header/></div>
                <div><AddTask triggerBackHomeState={this.props.triggerBackHomeState}/></div>
            </div>
        )
    }
}

export default Page