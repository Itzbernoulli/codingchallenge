import React, { Component } from 'react'
import Table from '../ListContainer/Table'
import Page from '../AddContainer/Page'
import axios from 'axios'

class Content extends Component {
    constructor(props){
        super(props)
        
        this.state = {
            task_modules:[],
            clickedItems:[],
            unclickedItems: []
        }
    }

    componentDidMount(){

        var self = this.state

        axios.get('/tasks.json')
        .then(data => {

            let clickedItems = []
            let unclickedItems = []

            data.data.map((data) =>{
                data.clicked ?
                clickedItems.push({id: data.id, description:data.description, avatar_url: data.avatar_url, clicked: data.clicked, update_time: data.updated_at, username: data.username}) :
                unclickedItems.push({id: data.id, description:data.description, avatar_url: data.avatar_url, clicked: data.clicked, update_time: data.updated_at, username: data.username})
            })
            
            this.setState({clickedItems: clickedItems, unclickedItems:unclickedItems})
        })
        .catch(data => {
            debugger
        })
    }

    handleItemClicked(item, event){
        event.preventDefault()

        item.clicked = true 
        var self = this.state

        var index = event.target.getAttribute("data-index")
        axios.put('/tasks/'+ item.id+ '.json', {task: item})
        .then(data => {

            let updated_data = data.data 
            
            const unclickedItems = this.state.unclickedItems.filter(i => i.id !== item.id)
            this.setState({unclickedItems:unclickedItems})
            let clickedItems = [...this.state.clickedItems]
            clickedItems.push({id: updated_data.id, description:updated_data.description, avatar_url: updated_data.avatar_url, clicked: updated_data.clicked, update_time: updated_data.updated_at, username: updated_data.username})
            this.setState({clickedItems: clickedItems})
        })
        .catch(data => {
            debugger
        })
    }

    render(){
        return(
                <Table handleItemClicked={this.handleItemClicked.bind(this)} task_modules={[...this.state.clickedItems, ...this.state.unclickedItems] } triggerAddTripState={this.props.triggerAddTripState}/>
        )
    }
}

export default Content