import React, { Component } from 'react'
import Content from './Content'
import Page from '../AddContainer/Page'

class Home extends Component {
    constructor(props) {
        super(props)
        this.state = { isEmptyState: true }
      }

    triggerAddTripState = () => {
    this.setState({
        ...this.state,
        isEmptyState: false,
        isAddTripState: true
    })
    }

    triggerBackHomeState = () => {
        this.setState({
            ...this.state,
            isEmptyState: true,
            isAddTripState: false
        })
        }

    render(){
        return(
            <div>
        {this.state.isEmptyState && <Content triggerAddTripState={this.triggerAddTripState.bind(this)} />}

        {this.state.isAddTripState && <Page triggerBackHomeState={this.triggerBackHomeState.bind(this)}/>}
        </div>
        )
    }
}

export default Home