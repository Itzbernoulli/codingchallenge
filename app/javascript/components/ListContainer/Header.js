import React, { Component } from 'react'

 const ListHeader = (props) =>{
     return(
            <nav className="navbar navbar-expand-lg navbar-dark bg-primary">
                <h2 style={{color: "#fff"}}>Tasks</h2>
                <a className="nav-link" onClick={props.triggerAddTripState}><i className="fa fa-plus fa-2x " style={{color: "#fff"}} ></i></a>
            </nav>
     )
 }

 export default ListHeader