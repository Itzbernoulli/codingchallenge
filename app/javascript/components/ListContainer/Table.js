import React, { Component } from 'react'
import UnclickedItem from './unclickeditem'
import ClickedItem from './clickeditem'
import Header from './Header'

class Table extends Component {
    constructor(props){
        super(props)
    }

    render(){

        const items = this.props.task_modules.map( (data, index)=> {
            return (
                data.clicked ?
                <ClickedItem key={'dust' + data.id} avatar_url={data.avatar_url}  description={data.description} update_time={data.update_time} username={data.username}/> :
                <UnclickedItem handleItemClicked={this.props.handleItemClicked.bind(this, data)} key={'dust' + data.id} avatar_url={data.avatar_url} description={data.description} index={index} username={data.username}/>
            ) 
        })

        return(
            <div>
                <div><Header triggerAddTripState={this.props.triggerAddTripState}/></div>
                <div>{items} </div>
            </div>
        )
    }
}

export default Table