import React, { Component } from 'react'
import styled from 'styled-components'

const Image = styled.img`
width:100px;
height:100px;
margin:10px 10px;
`

const ClickedItem = (props) =>  {

const date = new Date(props.update_time)
const localTimeString = date.toLocaleTimeString(undefined, {
	hour: '2-digit',
	minute: '2-digit'
})

    return(
        <div className="media">
          <Image src={props.avatar_url} alt=""/>
            <div className="media-body align-self-center">
                <h5 className="mt-0">{props.username}</h5>
                {props.description}
            </div>
                <p className="align-self-center ml-5 mt-5">{localTimeString}</p>
        </div>
    )
}

export default ClickedItem