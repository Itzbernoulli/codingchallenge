import React, { Component } from 'react'
import styled from 'styled-components'


const Image = styled.img`
width:100px;
height:100px;
margin:10px 10px;
`
const UnclickedItem = (props) => {

    return(
        <div className="media">
          <Image src={props.avatar_url} alt=""/>
            <div className="media-body align-self-center">
                <h5 className="mt-0">{props.username}</h5>
                {props.description}
            </div>
            <div className="mr-4">
            <input
            data-index={props.index}
            className="align-self-center ml-5 mt-5" 
            type="checkbox"
                checked={props.clicked}
                onChange={props.handleItemClicked}
                />
            </div>     
        </div>
    )
}

export default UnclickedItem