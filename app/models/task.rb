class Task < ApplicationRecord
    validates :username, :description, :avatar_url, presence: true
end
