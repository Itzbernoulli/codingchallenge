json.extract! task, :id, :username, :description, :avatar_url, :clicked, :created_at, :updated_at
json.url task_url(task, format: :json)
