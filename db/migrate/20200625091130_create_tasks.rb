class CreateTasks < ActiveRecord::Migration[6.0]
  def change
    create_table :tasks do |t|
      t.string :username
      t.string :description
      t.string :avatar_url
      t.boolean :clicked, default: false

      t.timestamps
    end
  end
end
