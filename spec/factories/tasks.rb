FactoryBot.define do
  factory :task do
    username { Faker::Name.name }
    description { Faker::Quote.yoda }
    avatar_url { "https://w7.pngwing.com/pngs/312/283/png-transparent-man-s-face-avatar-computer-icons-user-profile-business-user-avatar-blue-face-heroes.png" }
    clicked { false }
  end
end
