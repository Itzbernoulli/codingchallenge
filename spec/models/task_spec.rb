require 'rails_helper'

RSpec.describe Task, type: :model do
  it 'is valid with a username, description, avatar_url and a clicked status' do
    task = FactoryBot.build(:task)
    
    expect(task).to be_valid
  end
  
  it 'is invalid without a username' do
    task = FactoryBot.build(:task, username: nil)
    task.valid?

    expect(task.errors[:username]).to include("can't be blank")
  end

  it 'is invalid without a description' do
    task = FactoryBot.build(:task, description: nil)
    task.valid?

    expect(task.errors[:description]).to include("can't be blank")
  end

  it 'is invalid without an avatar_url' do
    task = FactoryBot.build(:task, avatar_url: nil)
    task.valid?

    expect(task.errors[:avatar_url]).to include("can't be blank")
  end
end
